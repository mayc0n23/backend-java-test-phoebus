package com.phoebus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendJavaTestPhoebusApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendJavaTestPhoebusApplication.class, args);
	}

}
