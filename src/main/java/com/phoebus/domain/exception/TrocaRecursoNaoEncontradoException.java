package com.phoebus.domain.exception;

public class TrocaRecursoNaoEncontradoException extends EntidadeNaoEncontradaException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TrocaRecursoNaoEncontradoException(String mensagem) {
		super(mensagem);
	}
	
	public TrocaRecursoNaoEncontradoException(Long trocaId) {
		this(String.format("Não foi encontrada nenhuma troca de recursos com o código '%d'", trocaId));
	}

}