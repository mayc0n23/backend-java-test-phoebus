package com.phoebus.domain.exception;

public class RecursoNaoEncontradoException extends EntidadeNaoEncontradaException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RecursoNaoEncontradoException(String mensagem) {
		super(mensagem);
	}

	public RecursoNaoEncontradoException(Long recursoId) {
		this(String.format("Recurso de código '%d' não foi encontrado", recursoId));
	}
	
}