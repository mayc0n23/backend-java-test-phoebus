package com.phoebus.domain.exception;

public class ItemNaoEncontradoException extends EntidadeNaoEncontradaException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ItemNaoEncontradoException(String mensagem) {
		super(mensagem);
	}
	
	public ItemNaoEncontradoException(Long itemId) {
		this(String.format("Item de código '%d' não encontrado.", itemId));
	}

}