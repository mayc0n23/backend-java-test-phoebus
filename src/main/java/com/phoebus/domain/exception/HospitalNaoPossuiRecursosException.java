package com.phoebus.domain.exception;

public class HospitalNaoPossuiRecursosException extends NegocioException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public HospitalNaoPossuiRecursosException(String mensagem) {
		super(mensagem);
	}
	
}