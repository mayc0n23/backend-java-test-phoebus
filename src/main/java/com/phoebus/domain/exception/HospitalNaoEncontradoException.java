package com.phoebus.domain.exception;

public class HospitalNaoEncontradoException extends EntidadeNaoEncontradaException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public HospitalNaoEncontradoException(String mensagem) {
		super(mensagem);
	}
	
	public HospitalNaoEncontradoException(Long hospitalId) {
		this(String.format("Hospital com código '%d' não foi encontrado.", hospitalId));
	}

}