package com.phoebus.domain.model;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.CreationTimestamp;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
public class TrocaRecursos { //Entidade irá representar a troca de recursos entre hospitais, servirá como histórico
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@EqualsAndHashCode.Include
	private Long id;
	
	@OneToOne
	private Hospital firstHospital;
	
	@OneToOne
	private Hospital secondHospital;
	
	@OneToMany
	private List<Recurso> firstHospitalResources = new ArrayList<>();
	
	@OneToMany
	private List<Recurso> secondHospitalResources = new ArrayList<>();
	
	@CreationTimestamp
	@Column(nullable = false, columnDefinition = "datetime")
	private OffsetDateTime dataTroca;
	
}