package com.phoebus.domain.model;

import javax.persistence.Embeddable;

import lombok.Data;

@Data
@Embeddable
public class Localizacao {
	
	private String latitude;
	
	private String longitude;
	
}