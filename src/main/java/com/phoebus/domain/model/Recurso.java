package com.phoebus.domain.model;

import java.math.BigInteger;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
public class Recurso { //Representa um recurso e a quantidade desse recurso no hospital
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@EqualsAndHashCode.Include
	private Long id;
	
	@OneToOne
	private Item item;
	
	private BigInteger quantidade;
	
	public BigInteger getTotalPontos() {
		return this.item.getPontos().multiply(this.quantidade);
	}
	
}