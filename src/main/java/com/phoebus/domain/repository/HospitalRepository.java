package com.phoebus.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.phoebus.domain.model.Hospital;

@Repository
public interface HospitalRepository extends JpaRepository<Hospital, Long> {
	
	//Consulta que trás os hospitais que possuem o percentual de ocupação menor que 90
	@Query("from Hospital where percentualOcupacao < 90 order by dataAtualizacao asc")
	List<Hospital> baixaLotacao();
	
	//Consulta que trás os hospitais que possuem o percentual de ocupação maior que 90
	@Query("from Hospital where percentualOcupacao > 90 order by dataAtualizacao asc")
	List<Hospital> superLotacao();
	
	//Retorna a porcentagem de hospitais que estão em super lotação
	@Query(value = "SELECT round(((lotados*100)/total), 2) as porcentagem from (SELECT sum(case when percentual_ocupacao > 90 then 1 else 0 end) as lotados, " + 
			"count(*) as total FROM phoebustest.hospital) as resultado;", nativeQuery = true)
	Double porcentagemHospitaisSuperLotacao();
	
	//Retorna a porcentagem de hospitais que estão em baixa lotação
	@Query(value = "SELECT round(((lotados*100)/total), 2) as porcentagem from (SELECT sum(case when percentual_ocupacao < 90 then 1 else 0 end) as lotados, " + 
			"count(*) as total FROM phoebustest.hospital) as resultado;", nativeQuery = true)
	Double porcentagemHospitaisBaixaLotacao();
	
}