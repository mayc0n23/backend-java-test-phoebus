package com.phoebus.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.phoebus.domain.model.Recurso;

@Repository
public interface RecursoRepository extends JpaRepository<Recurso, Long> {

}