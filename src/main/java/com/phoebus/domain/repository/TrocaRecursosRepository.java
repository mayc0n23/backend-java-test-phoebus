package com.phoebus.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.phoebus.domain.model.TrocaRecursos;

@Repository
public interface TrocaRecursosRepository extends JpaRepository<TrocaRecursos, Long> {

}