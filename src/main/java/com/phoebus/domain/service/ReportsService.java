package com.phoebus.domain.service;

import java.util.Collection;

public interface ReportsService {
	
	byte[] emitirRelatorio(String path, Collection<?> collection);
	
}