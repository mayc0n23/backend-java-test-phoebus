package com.phoebus.domain.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.phoebus.domain.exception.HospitalNaoEncontradoException;
import com.phoebus.domain.model.Hospital;
import com.phoebus.domain.repository.HospitalRepository;

@Service
public class HospitalService {
	
	@Autowired
	private HospitalRepository hospitalRepository;
	
	@Transactional
	public Hospital salvar(Hospital hospital) {
		return hospitalRepository.save(hospital);
	}
	
	//Busca um hospital pelo ID, caso encontre, então retorna o hospital, caso não, lança uma exceção
	public Hospital buscarOuFalhar(Long hospitalId) {
		return hospitalRepository.findById(hospitalId)
				.orElseThrow(() -> new HospitalNaoEncontradoException(hospitalId));
	}
	
	public List<Hospital> buscarTodos() {
		return hospitalRepository.findAll();
	}
	
	public List<Hospital> baixaLotacao() {
		return hospitalRepository.baixaLotacao();
	}
	
	public List<Hospital> superLotacao() {
		return hospitalRepository.superLotacao();
	}
	
	public Double porcentagemBaixaLotacao() {
		return hospitalRepository.porcentagemHospitaisBaixaLotacao();
	}
	
	public Double porcentagemSuperLotacao() {
		return hospitalRepository.porcentagemHospitaisSuperLotacao();
	}
	
}