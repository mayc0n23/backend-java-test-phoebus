package com.phoebus.domain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.phoebus.domain.exception.RecursoNaoEncontradoException;
import com.phoebus.domain.model.Recurso;
import com.phoebus.domain.repository.RecursoRepository;

@Service
public class RecursoService {
	
	@Autowired
	private RecursoRepository recursoRepository;
	
	public Recurso buscarOuFalhar(Long recursoId) {
		return recursoRepository.findById(recursoId)
				.orElseThrow(() -> new RecursoNaoEncontradoException(recursoId));
	}
	
}