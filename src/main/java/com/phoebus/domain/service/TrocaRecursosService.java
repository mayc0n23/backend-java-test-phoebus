package com.phoebus.domain.service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.phoebus.domain.exception.HospitalNaoPossuiRecursosException;
import com.phoebus.domain.exception.NegocioException;
import com.phoebus.domain.model.Hospital;
import com.phoebus.domain.model.Recurso;
import com.phoebus.domain.model.TrocaRecursos;
import com.phoebus.domain.repository.HospitalRepository;
import com.phoebus.domain.repository.TrocaRecursosRepository;

/*
 * A parte de intercambio de recursos não ficou claro para mim, mandei um email para esclarecer as duvidas mas não me retornaram
 * Então eu decidi criar essa implementação, não sei se é como esperavam, mas funciona =)
 */

@Service
public class TrocaRecursosService {
	
	@Autowired
	private TrocaRecursosRepository trocaRecursosRepository;
	
	@Autowired
	private HospitalService hospitalService;
	
	@Autowired
	private RecursoService recursoService;
	
	@Autowired
	private HospitalRepository hospitalRepository;
	
	public List<TrocaRecursos> listar() {
		return trocaRecursosRepository.findAll();
	}
	
	@Transactional
	public TrocaRecursos trocar(Long firstHospitalId, Long secondHospitalId, TrocaRecursos trocaRecursos) {
		Hospital firstHospital = hospitalService.buscarOuFalhar(firstHospitalId);
		Hospital secondHospital = hospitalService.buscarOuFalhar(secondHospitalId);
		
		trocaRecursos.setFirstHospital(firstHospital);
		trocaRecursos.setSecondHospital(secondHospital);
		
		List<Recurso> firstResources = new ArrayList<>();
		for(Recurso recurso: trocaRecursos.getFirstHospitalResources()) {
			firstResources.add(recursoService.buscarOuFalhar(recurso.getId()));
		}
		
		List<Recurso> secondResources = new ArrayList<>();
		for(Recurso recurso: trocaRecursos.getSecondHospitalResources()) {
			secondResources.add(recursoService.buscarOuFalhar(recurso.getId()));
		}
		
		if (!hospitalPossuiRecurso(firstHospital, firstResources) || 
				!hospitalPossuiRecurso(secondHospital, secondResources)) {
			throw new HospitalNaoPossuiRecursosException("Algum dos hospitais não possuem os recursos informados.");
		}
		
		if (!validarTrocaPorPercentual(firstHospital, secondHospital)) {
			if (!mesmaQuantidadeDePontos(firstResources, secondResources)) {
				throw new NegocioException("Os hospitais não forneceram a mesma quantidade de pontos.");
			}
		}
		
		firstHospital.getRecursos().removeAll(firstResources);
		secondHospital.getRecursos().removeAll(secondResources);
		hospitalRepository.flush();
		
		firstHospital.getRecursos().addAll(secondResources);
		secondHospital.getRecursos().addAll(firstResources);
		
		return trocaRecursosRepository.save(trocaRecursos);
	}
	
	private boolean validarTrocaPorPercentual(Hospital firstHospital, Hospital secondHospital) {
		return (firstHospital.getPercentualOcupacao().compareTo(new BigDecimal(90)) == 1) || 
				(secondHospital.getPercentualOcupacao().compareTo(new BigDecimal(90)) == 1);
	}
	
	private boolean hospitalPossuiRecurso(Hospital hospital, List<Recurso> recursos) {
		for(Recurso recurso: recursos) {
			if (!hospital.getRecursos().contains(recurso)) {
				return false;
			}
		}
		return true;
	}
	
	private boolean mesmaQuantidadeDePontos(List<Recurso> firstResources, List<Recurso> secondResources) {
		BigInteger totalPontosFirstHospital = BigInteger.ZERO;
		BigInteger totalPontosSecondHospital = BigInteger.ZERO;
		for(Recurso recurso: firstResources) {
			totalPontosFirstHospital =  totalPontosFirstHospital.add(recurso.getTotalPontos());
		}
		for(Recurso recurso: secondResources) {
			totalPontosSecondHospital =  totalPontosSecondHospital.add(recurso.getTotalPontos());
		}
		return totalPontosFirstHospital.compareTo(totalPontosSecondHospital) == 0;
	}
	
}