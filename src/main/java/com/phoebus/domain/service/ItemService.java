package com.phoebus.domain.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.phoebus.domain.exception.ItemNaoEncontradoException;
import com.phoebus.domain.model.Item;
import com.phoebus.domain.repository.ItemRepository;

@Service
public class ItemService {
	
	@Autowired
	private ItemRepository itemRepository;
	
	@Transactional
	public Item salvar(Item item) {
		return itemRepository.save(item);
	}
	
	public List<Item> buscarTodos() {
		return itemRepository.findAll();
	}
	
	public Item buscarOuFalhar(Long itemId) {
		return itemRepository.findById(itemId)
				.orElseThrow(() -> new ItemNaoEncontradoException(itemId));
	}
	
}