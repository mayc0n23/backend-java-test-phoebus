package com.phoebus.api.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.phoebus.api.assembler.ItemInputDisassembler;
import com.phoebus.api.assembler.ItemModelAssembler;
import com.phoebus.api.model.input.ItemInputModel;
import com.phoebus.api.model.output.ItemModel;
import com.phoebus.domain.model.Item;
import com.phoebus.domain.service.ItemService;

@RestController
@RequestMapping("/itens")
public class ItemController {
	
	@Autowired
	private ItemService itemService;
	
	@Autowired
	private ItemModelAssembler itemModelAssembler;
	
	@Autowired
	private ItemInputDisassembler itemInputDisassembler;
	
	@GetMapping
	public List<ItemModel> listar() {
		return itemModelAssembler.toCollectionModel(itemService.buscarTodos());
	}
	
	@GetMapping("/{itemId}")
	public ItemModel buscar(@PathVariable Long itemId) {
		return itemModelAssembler.toModel(itemService.buscarOuFalhar(itemId));
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ItemModel adicionar(@RequestBody @Valid ItemInputModel item) {
		return itemModelAssembler.toModel(itemService.salvar(itemInputDisassembler.toDomainObject(item)));
	}
	
	@PutMapping("/{itemId}")
	public ItemModel atualizar(@PathVariable Long itemId, @RequestBody @Valid ItemInputModel item) {
		Item itemAtual = itemService.buscarOuFalhar(itemId);
		itemInputDisassembler.copyToDomainObject(item, itemAtual);
		return itemModelAssembler.toModel(itemService.salvar(itemAtual));
	}

}