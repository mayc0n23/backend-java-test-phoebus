package com.phoebus.api.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.phoebus.api.assembler.HistoricoNegociacaoAssembler;
import com.phoebus.api.model.output.PorcentagemBaixaSuperLotacao;
import com.phoebus.domain.service.HospitalService;
import com.phoebus.domain.service.ReportsService;
import com.phoebus.domain.service.TrocaRecursosService;

@RestController
@RequestMapping("/relatorios")
public class RelatoriosController {
	
	@Autowired
	private HospitalService hospitalService;
	
	@Autowired
	private TrocaRecursosService trocaRecursoService;
	
	@Autowired
	private HistoricoNegociacaoAssembler historicoNegociacaoAssembler;
	
	@Autowired
	private ReportsService historicoNegociacaoReportsService;
	
	@GetMapping(path = "/historico-negociacao", produces = MediaType.APPLICATION_PDF_VALUE)
	public ResponseEntity<byte[]> historicoNegociacao() {
		
		byte[] bytesPdf = historicoNegociacaoReportsService.emitirRelatorio("/relatorios/relatorios.jasper",
				historicoNegociacaoAssembler.toCollectionModel(trocaRecursoService.listar()));
		
		var headers = new HttpHeaders();
		//Indica que o conteudo como resposta (no caso o pdf) deve ser baixado pelo cliente e o nome do arquivo baixado
		headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=historico-negociacao.pdf");
		
		return ResponseEntity.ok()
				.contentType(MediaType.APPLICATION_PDF)
				.headers(headers)
				.body(bytesPdf);
	}
	
	@GetMapping(path = "/baixa-lotacao", produces = MediaType.APPLICATION_PDF_VALUE)
	public ResponseEntity<byte[]> baixaLotacao() {
		
		byte[] bytesPdf = historicoNegociacaoReportsService.emitirRelatorio("/relatorios/ocupacao-hospitais.jasper",
				hospitalService.baixaLotacao());
		
		var headers = new HttpHeaders();
		//Indica que o conteudo como resposta (no caso o pdf) deve ser baixado pelo cliente e o nome do arquivo baixado
		headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=baixa-lotacao.pdf");
		
		return ResponseEntity.ok()
				.contentType(MediaType.APPLICATION_PDF)
				.headers(headers)
				.body(bytesPdf);
	}
	
	@GetMapping(path = "/super-lotacao", produces = MediaType.APPLICATION_PDF_VALUE)
	public ResponseEntity<byte[]> superLotacao() {
		
		byte[] bytesPdf = historicoNegociacaoReportsService.emitirRelatorio("/relatorios/ocupacao-hospitais.jasper",
				hospitalService.superLotacao());
		
		var headers = new HttpHeaders();
		//Indica que o conteudo como resposta (no caso o pdf) deve ser baixado pelo cliente e o nome do arquivo baixado
		headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=super-lotacao.pdf");
		
		return ResponseEntity.ok()
				.contentType(MediaType.APPLICATION_PDF)
				.headers(headers)
				.body(bytesPdf);
	}
	
	@GetMapping(path = "/porcentagem-hospitais-baixa-super-lotacao", produces = MediaType.APPLICATION_PDF_VALUE)
	public ResponseEntity<byte[]> porcentagemHospitaisBaixaSuperLotacao() {
		
		Double porcentagemBaixaLotacao = hospitalService.porcentagemBaixaLotacao();
		Double porcentagemSuperLotacao = hospitalService.porcentagemSuperLotacao();
		PorcentagemBaixaSuperLotacao dado = new PorcentagemBaixaSuperLotacao();
		dado.setBaixaLotacao(porcentagemBaixaLotacao);
		dado.setSuperLotacao(porcentagemSuperLotacao);
		List<PorcentagemBaixaSuperLotacao> dados = new ArrayList<>();
		dados.add(dado);
		byte[] bytesPdf = historicoNegociacaoReportsService.emitirRelatorio("/relatorios/porcentagem-baixa-super-lotacao.jasper",
				dados);
		
		var headers = new HttpHeaders();
		//Indica que o conteudo como resposta (no caso o pdf) deve ser baixado pelo cliente e o nome do arquivo baixado
		headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=porcentagem-baixa-super-lotacao.pdf");
		
		return ResponseEntity.ok()
				.contentType(MediaType.APPLICATION_PDF)
				.headers(headers)
				.body(bytesPdf);
	}
	
	
}