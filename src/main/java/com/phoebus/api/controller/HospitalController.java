package com.phoebus.api.controller;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.phoebus.api.assembler.HospitalInputDisassembler;
import com.phoebus.api.assembler.HospitalModelAssembler;
import com.phoebus.api.model.input.HospitalInputModel;
import com.phoebus.api.model.output.HospitalModel;
import com.phoebus.domain.model.Hospital;
import com.phoebus.domain.service.HospitalService;

@RestController
@RequestMapping("/hospitais")
public class HospitalController {
	
	@Autowired
	private HospitalService hospitalService;
	
	@Autowired
	private HospitalModelAssembler hospitalModelAssembler;
	
	@Autowired
	private HospitalInputDisassembler hospitalInputDisassembler;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@GetMapping
	public List<HospitalModel> listar() {
		return hospitalModelAssembler.toCollectionModel(hospitalService.buscarTodos());
	}
	
	@GetMapping("/{hospitalId}")
	public HospitalModel buscar(@PathVariable Long hospitalId) {
		return hospitalModelAssembler.toModel(hospitalService.buscarOuFalhar(hospitalId));
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public HospitalModel adicionar(@RequestBody @Valid HospitalInputModel hospital) {
		Hospital hospitalSalvo = hospitalService.salvar(hospitalInputDisassembler.toDomainObject(hospital));
		return hospitalModelAssembler.toModel(hospitalSalvo);
	}
	
	private HospitalModel atualizar(@PathVariable Long hospitalId, Hospital hospital) {
		Hospital hospitalAtual = hospitalService.buscarOuFalhar(hospitalId);
		modelMapper.map(hospital, hospitalAtual);
		return hospitalModelAssembler.toModel(hospitalService.salvar(hospitalAtual));
	}
	
	@PatchMapping("/{hospitalId}")
	public HospitalModel atualizarOcupacao(@PathVariable Long hospitalId, @RequestBody Map<String, Object> campos, 
			HttpServletRequest request) {
		Hospital hospital = hospitalService.buscarOuFalhar(hospitalId);
		hospital.setDataAtualizacao(new Date());
		merge(campos, hospital, request);
		return atualizar(hospitalId, hospital);
	}
	
	//Usado para fazer o patch, pois só é passado 1 valor para atualização e precisa ser feita a mesclagem
	//Com os dados já existentes no banco de dados
	private void merge(Map<String, Object> origem, Hospital destino, HttpServletRequest request) {
		ServletServerHttpRequest serverHttpRequest = new ServletServerHttpRequest(request);
		
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, true);
			objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, true);
			Hospital hospitalOrigem = objectMapper.convertValue(origem, Hospital.class);
			
			//Percorre os campos que chegaram no corpo da requisição
			origem.forEach((nomePropriedade, valorPropriedade) -> {
				Field field = ReflectionUtils.findField(Hospital.class, nomePropriedade);
				field.setAccessible(true);
				
				Object novoValor = ReflectionUtils.getField(field, hospitalOrigem);
				
				ReflectionUtils.setField(field, destino, novoValor);
			});
		} catch (IllegalArgumentException ex) {
			Throwable rootCause = ExceptionUtils.getRootCause(ex);
			
			throw new HttpMessageNotReadableException(ex.getMessage(), rootCause, serverHttpRequest);
		}
	}
	
}