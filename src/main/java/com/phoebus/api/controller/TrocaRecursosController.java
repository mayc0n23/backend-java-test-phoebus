package com.phoebus.api.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.phoebus.api.assembler.TrocaRecursosInputDisassembler;
import com.phoebus.api.assembler.TrocaRecursosModelAssembler;
import com.phoebus.api.model.input.TrocaRecursosInputModel;
import com.phoebus.api.model.output.TrocaRecursosModel;
import com.phoebus.domain.service.TrocaRecursosService;

@RestController
@RequestMapping("/troca-recursos")
public class TrocaRecursosController {
	
	@Autowired
	private TrocaRecursosService trocaRecursosService;
	
	@Autowired
	private TrocaRecursosModelAssembler trocaRecursosModelAssembler;
	
	@Autowired
	private TrocaRecursosInputDisassembler trocaRecursosInputDisassembler;
	
	@PostMapping
	public TrocaRecursosModel trocar(@RequestParam("first_hospital_id") Long firstHospitalId, 
			@RequestParam("second_hospital_id") Long secondHospitalId,
			@RequestBody @Valid TrocaRecursosInputModel trocaInput) {
		return trocaRecursosModelAssembler.toModel(trocaRecursosService.trocar(firstHospitalId, secondHospitalId, 
				trocaRecursosInputDisassembler.toDomainObject(trocaInput)));
	}
	
}