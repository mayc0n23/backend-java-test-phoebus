package com.phoebus.api.model.input;

import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class LocalizacaoInputModel {
	
	@NotBlank
	private String latitude;
	
	@NotBlank
	private String longitude;
	
}