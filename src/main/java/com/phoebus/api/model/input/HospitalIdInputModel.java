package com.phoebus.api.model.input;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class HospitalIdInputModel {
	
	@NotNull
	private Long id;
	
}