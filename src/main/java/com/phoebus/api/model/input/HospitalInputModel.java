package com.phoebus.api.model.input;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class HospitalInputModel {
	
	@NotBlank
	private String nome;
	
	@NotBlank
	private String cnpj;
	
	@PositiveOrZero
	private BigDecimal percentualOcupacao;
	
	@Valid
	@NotNull
	private EnderecoInputModel endereco;
	
	@Valid
	@NotNull
	private LocalizacaoInputModel localizacao;
	
	@Valid
	private List<RecursoInputModel> recursos = new ArrayList<>();
	
}