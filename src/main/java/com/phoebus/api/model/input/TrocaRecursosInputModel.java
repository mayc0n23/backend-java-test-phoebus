package com.phoebus.api.model.input;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TrocaRecursosInputModel {
	
	private List<RecursoIdInputModel> firstHospitalResources = new ArrayList<>();
	
	private List<RecursoIdInputModel> secondHospitalResources = new ArrayList<>();
}