package com.phoebus.api.model.input;

import java.math.BigInteger;

import javax.validation.Valid;
import javax.validation.constraints.PositiveOrZero;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class RecursoInputModel {
	
	@Valid
	private ItemIdInputModel item;
	
	@PositiveOrZero
	private BigInteger quantidade;
	
}