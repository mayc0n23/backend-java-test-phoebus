package com.phoebus.api.model.input;

import java.math.BigInteger;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PositiveOrZero;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ItemInputModel {
	
	@NotBlank
	private String nome;
	
	@PositiveOrZero
	private BigInteger pontos;
	
}