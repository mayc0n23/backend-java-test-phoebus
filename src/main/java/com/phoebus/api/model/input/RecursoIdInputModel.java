package com.phoebus.api.model.input;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class RecursoIdInputModel {
	
	@NotNull
	private Long id;
	
}