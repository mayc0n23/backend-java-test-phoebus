package com.phoebus.api.model.output;

import java.time.OffsetDateTime;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TrocaRecursosModel {
	
	private Long id;
	
	private Long firstHospitalId;
	
	private Long secondHospitalId;
	
	private OffsetDateTime dataTroca;
	
}