package com.phoebus.api.model.output;

import java.math.BigDecimal;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class HospitalModel {
	
	private Long id;
	
	private String nome;
	
	private String cnpj;
	
	private BigDecimal percentualOcupacao;
	
	private EnderecoModel endereco;
	
	private LocalizacaoModel localizacao;
	
	private List<RecursoModel> recursos;
	
}