package com.phoebus.api.model.output;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class LocalizacaoModel {
	
	private String latitude;
	
	private String longitude;
	
}