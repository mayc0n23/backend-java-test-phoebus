package com.phoebus.api.model.output;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PorcentagemBaixaSuperLotacao {
	
	private Double baixaLotacao;
	
	private Double superLotacao;
	
}