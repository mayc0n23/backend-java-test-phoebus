package com.phoebus.api.model.output;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class HistoricoNegociacaoModel {
	
	private String nomeFirstHospital;
	
	private String nomeSecondHospital;
	
	private Date dataTroca;
	
}