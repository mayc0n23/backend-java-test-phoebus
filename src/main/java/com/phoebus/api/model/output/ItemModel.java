package com.phoebus.api.model.output;

import java.math.BigInteger;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ItemModel {
	
	private Long id;
	
	private String nome;
	
	private BigInteger pontos;
	
}