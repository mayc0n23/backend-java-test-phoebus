package com.phoebus.api.model.output;

import java.math.BigInteger;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class RecursoModel {
	
	private ItemModel item;
	
	private BigInteger quantidade;
	
}