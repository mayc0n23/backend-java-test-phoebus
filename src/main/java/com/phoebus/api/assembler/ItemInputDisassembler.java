package com.phoebus.api.assembler;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.phoebus.api.model.input.ItemInputModel;
import com.phoebus.domain.model.Item;

@Component
public class ItemInputDisassembler {
	
	@Autowired
	private ModelMapper modelMapper;
	
	public Item toDomainObject(ItemInputModel itemInput) {
		return modelMapper.map(itemInput, Item.class);
	}
	
	public void copyToDomainObject(ItemInputModel itemInput, Item item) {
		modelMapper.map(itemInput, item);
	}
	
}