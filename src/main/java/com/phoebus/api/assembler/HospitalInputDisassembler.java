package com.phoebus.api.assembler;

import java.util.ArrayList;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.phoebus.api.model.input.HospitalInputModel;
import com.phoebus.domain.model.Hospital;
import com.phoebus.domain.model.Recurso;

@Component
public class HospitalInputDisassembler {
	
	@Autowired
	private ModelMapper modelMapper;
	
	public Hospital toDomainObject(HospitalInputModel hospital) {
		return modelMapper.map(hospital, Hospital.class);
	}
	
	public void copyToDomainObject(HospitalInputModel hospitalInput, Hospital hospital) {
		hospital.setRecursos(new ArrayList<Recurso>());
		modelMapper.map(hospitalInput, hospital);
	}
	
}