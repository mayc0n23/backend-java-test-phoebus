package com.phoebus.api.assembler;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.phoebus.api.model.output.HospitalModel;
import com.phoebus.domain.model.Hospital;

@Component
public class HospitalModelAssembler {
	
	@Autowired
	private ModelMapper modelMapper;
	
	public HospitalModel toModel(Hospital hospital) {
		return modelMapper.map(hospital, HospitalModel.class);
	}
	
	public List<HospitalModel> toCollectionModel(List<Hospital> hospitais) {
		return hospitais.stream()
				.map(hospital -> toModel(hospital))
				.collect(Collectors.toList());
	}
	
}