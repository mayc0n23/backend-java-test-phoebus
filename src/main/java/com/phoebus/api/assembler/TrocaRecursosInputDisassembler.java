package com.phoebus.api.assembler;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.phoebus.api.model.input.TrocaRecursosInputModel;
import com.phoebus.domain.model.TrocaRecursos;

@Component
public class TrocaRecursosInputDisassembler {
	
	@Autowired
	private ModelMapper modelMapper;
	
	public TrocaRecursos toDomainObject(TrocaRecursosInputModel trocaRecursos) {
		return modelMapper.map(trocaRecursos, TrocaRecursos.class);
	}
	
	public void copyToDomainObject(TrocaRecursosInputModel trocaInput, TrocaRecursos trocaRecursos) {
		modelMapper.map(trocaInput, trocaRecursos);
	}
	
}