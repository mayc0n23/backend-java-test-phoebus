package com.phoebus.api.assembler;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.phoebus.api.model.output.TrocaRecursosModel;
import com.phoebus.domain.model.TrocaRecursos;

@Component
public class TrocaRecursosModelAssembler {

	@Autowired
	private ModelMapper modelMapper;
	
	public TrocaRecursosModel toModel(TrocaRecursos trocaRecursos) {
		return modelMapper.map(trocaRecursos, TrocaRecursosModel.class);
	}
	
	public List<TrocaRecursosModel> toCollectionModel(List<TrocaRecursos> trocaRecursos) {
		return trocaRecursos.stream()
				.map(trocaRecurso -> toModel(trocaRecurso))
				.collect(Collectors.toList());
	}
	
}