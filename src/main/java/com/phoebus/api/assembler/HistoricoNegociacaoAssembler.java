package com.phoebus.api.assembler;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.phoebus.api.model.output.HistoricoNegociacaoModel;
import com.phoebus.domain.model.TrocaRecursos;

@Component
public class HistoricoNegociacaoAssembler {
	
	@Autowired
	private ModelMapper modelMapper;
	
	public HistoricoNegociacaoModel toModel(TrocaRecursos trocaRecursos) {
		return modelMapper.map(trocaRecursos, HistoricoNegociacaoModel.class);
	}
	
	public List<HistoricoNegociacaoModel> toCollectionModel(List<TrocaRecursos> trocaRecursos) {
		return trocaRecursos.stream()
				.map(troca -> toModel(troca))
				.collect(Collectors.toList());
	}
	
}