package com.phoebus.api.exceptionhandler;

import java.time.OffsetDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Builder;
import lombok.Getter;

@JsonInclude(Include.NON_NULL)
@Builder
@Getter
public class Problem { //Classe que representa o retorno da requisição quando houver erros (implementa o padrão Problem Details)
	
	private Integer status;
	
	private String tipo;
	
	private String titulo;
	
	private String detalhe;
	
	private OffsetDateTime timestamp;
	
	private List<Object> objects;
	
	@Builder
	@Getter
	public static class Object {
		private String nome;
		private String mensagem;
	}
	
}