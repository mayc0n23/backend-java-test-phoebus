package com.phoebus.api.exceptionhandler;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.fasterxml.jackson.databind.exc.PropertyBindingException;
import com.phoebus.domain.exception.EntidadeNaoEncontradaException;
import com.phoebus.domain.exception.NegocioException;

//Classe que trata todas as exceções que são lançadas
@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {
	
	@Autowired
	private MessageSource messageSource;
	
	private static final String MSG_ERRO_GENERICA_USUARIO_FINAL = "Ocorreu um erro interno inesperado no sistema. Tente novamente e se o "
			+ "problema persistir, entre em contato com o administrador do sistema.";
	
	@ExceptionHandler(NegocioException.class)
	public ResponseEntity<?> handleNegocioException(NegocioException ex, WebRequest request) {
		HttpStatus status = HttpStatus.BAD_REQUEST;
		ProblemType problemType = ProblemType.ERRO_NEGOCIO;
		String detalhe = ex.getMessage();
		
		Problem problem = createProblemBuilder(status, problemType, detalhe).build();
		
		return handleExceptionInternal(ex, problem, new HttpHeaders(), status, request);
	}
	
	@ExceptionHandler(EntidadeNaoEncontradaException.class)
	public ResponseEntity<?> handleEntidadeNaoEncontradaException(EntidadeNaoEncontradaException ex, WebRequest request) {
		HttpStatus status = HttpStatus.NOT_FOUND;
		ProblemType problemType = ProblemType.RECURSO_NAO_ENCONTRADO;
		String detalhe = ex.getMessage();
		
		Problem problem = createProblemBuilder(status, problemType, detalhe).build();
		
		return handleExceptionInternal(ex, problem, new HttpHeaders(), status, request);
	}
	
	//Exceção que pode ser lançada quando a erros de sintaxe no corpo da requisição
		@Override
		protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
				HttpHeaders headers, HttpStatus status, WebRequest request) {

			Throwable rootCause = ExceptionUtils.getRootCause(ex);

			if (rootCause instanceof InvalidFormatException) {
				return handleInvalidFormatException((InvalidFormatException) rootCause, headers, status, request);
			} else if (rootCause instanceof PropertyBindingException) {
				return handlePropertyBidingException((PropertyBindingException) rootCause, headers, status, request);
			}

			ProblemType problemType = ProblemType.MENSAGEM_INCOMPREENSIVEL;
			String detalhe = "O corpo da requisição está inválido, verifique a sintaxe";

			Problem problem = createProblemBuilder(status, problemType, detalhe).build();

			return handleExceptionInternal(ex, problem, headers, status, request);
		}
	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {

		return handleValidationInternal(ex, ex.getBindingResult(), status, headers, request);
	}
	
	//Exceção lançada quando tentamos acessar um recurso inexistente
		@Override
		protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers,
				HttpStatus status, WebRequest request) {

			ProblemType problemType = ProblemType.RECURSO_NAO_ENCONTRADO;
			String detalhe = String.format("O recurso %s, que você tentou acessar, é inexistente.", ex.getRequestURL());

			Problem problem = createProblemBuilder(status, problemType, detalhe).build();

			return handleExceptionInternal(ex, problem, headers, status, request);
		}
	
	@Override
	protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {

		if (ex instanceof MethodArgumentTypeMismatchException) {
			return handleMethodArgumentTypeMismatch((MethodArgumentTypeMismatchException) ex, headers, status, request);
		}

		return super.handleTypeMismatch(ex, headers, status, request);
	}
	
	//Exceção lançada quando o usuario passa o parametro inválido na URL da requisição
		private ResponseEntity<Object> handleMethodArgumentTypeMismatch(MethodArgumentTypeMismatchException ex,
				HttpHeaders headers, HttpStatus status, WebRequest request) {

			ProblemType problemType = ProblemType.PARAMETRO_INVALIDO;

			String detalhe = String.format(
					"O parâmetro de URL '%s' recebeu o valor '%s', "
							+ "que é de um tipo inválido. Corrija e informe um valor compatível com o tipo %s.",
					ex.getName(), ex.getValue(), ex.getRequiredType().getSimpleName());

			Problem problem = createProblemBuilder(status, problemType, detalhe).build();

			return handleExceptionInternal(ex, problem, headers, status, request);
		}
	
	//Exceção lançada quando o usuario coloca uma propriedade inexistente na requisição
		private ResponseEntity<Object> handlePropertyBidingException(PropertyBindingException ex, HttpHeaders headers,
				HttpStatus status, WebRequest request) {

			String path = ex.getPath().stream().map(ref -> ref.getFieldName()).collect(Collectors.joining("."));

			ProblemType problemType = ProblemType.MENSAGEM_INCOMPREENSIVEL;
			String detalhe = String.format("A propriedade '%s' não existe. Corrija ou remova a propriedade.", path);

			Problem problem = createProblemBuilder(status, problemType, detalhe).build();

			return handleExceptionInternal(ex, problem, headers, status, request);
		}
	
	//Exceção lançada quando passamos um valor incorreto a uma propriedade
		private ResponseEntity<Object> handleInvalidFormatException(InvalidFormatException ex, HttpHeaders headers,
				HttpStatus status, WebRequest request) {

			String path = ex.getPath().stream().map(ref -> ref.getFieldName()).collect(Collectors.joining("."));

			ProblemType problemType = ProblemType.MENSAGEM_INCOMPREENSIVEL;
			String detalhe = String.format(
					"A propriedade '%s' recebeu o valor '%s' que é um tipo inválido. "
							+ "Corrija e informe um valor compatível com o tipo %s.",
					path, ex.getValue(), ex.getTargetType().getSimpleName());

			Problem problem = createProblemBuilder(status, problemType, detalhe).build();

			return handleExceptionInternal(ex, problem, headers, status, request);
		}
	
	//Exceção lançada quando falta algum campo obrigatorio no corpo da requisição
		private ResponseEntity<Object> handleValidationInternal(Exception ex, BindingResult bindingResult,
				HttpStatus status, HttpHeaders headers, WebRequest request) {

			ProblemType problemType = ProblemType.DADOS_INVALIDOS;
			String detalhe = "Um ou mais campos estão inválidos. Faça o preenchimento correto e tente novamente.";

			List<Problem.Object> problemObjects = bindingResult.getAllErrors().stream().map(objectError -> {
				String mensagem = messageSource.getMessage(objectError, LocaleContextHolder.getLocale());

				String nome = objectError.getObjectName();

				if (objectError instanceof FieldError) {
					nome = ((FieldError) objectError).getField();
				}

				return Problem.Object.builder().nome(nome).mensagem(mensagem).build();
			}).collect(Collectors.toList());

			Problem problem = createProblemBuilder(status, problemType, detalhe).detalhe(detalhe).objects(problemObjects)
					.build();

			return handleExceptionInternal(ex, problem, headers, status, request);

		}
	
	public ResponseEntity<?> handleGenericException(Exception ex, WebRequest request) {
		HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
		ProblemType problemType = ProblemType.ERRO_SISTEMA;
		String detalhe = MSG_ERRO_GENERICA_USUARIO_FINAL;

		Problem problem = createProblemBuilder(status, problemType, detalhe).build();

		return handleExceptionInternal(ex, problem, new HttpHeaders(), status, request);
	}
	
	@Override
	protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers,
			HttpStatus status, WebRequest request) {

		if (body == null) {
			body = Problem.builder().detalhe(MSG_ERRO_GENERICA_USUARIO_FINAL).titulo(status.getReasonPhrase())
					.status(status.value()).build();
		} else if (body instanceof String) {
			body = Problem.builder().detalhe(MSG_ERRO_GENERICA_USUARIO_FINAL).titulo((String) body)
					.status(status.value()).build();
		}

		return super.handleExceptionInternal(ex, body, headers, status, request);
	}
	
	private Problem.ProblemBuilder createProblemBuilder(HttpStatus status, ProblemType problemType, String detail) {
		return Problem.builder().timestamp(OffsetDateTime.now()).status(status.value()).tipo(problemType.getUri())
				.titulo(problemType.getTitle()).detalhe(detail);
	}
	
}