package com.phoebus.api.exceptionhandler;

import lombok.Getter;

@Getter
public enum ProblemType {
	
	ERRO_NEGOCIO("Viola regra de negócio", "/erro-de-negocio"),
	
	RECURSO_NAO_ENCONTRADO("Recurso não encontrado", "/recurso-nao-encontrado"),
	
	PARAMETRO_INVALIDO("Parametro inválido", "/parametro-invalido"),
	
	MENSAGEM_INCOMPREENSIVEL("Mensagem incompreensivel", "/mensagem-incompreensivel"),
	
	ERRO_SISTEMA("Erro no sistema", "/erro-sistema"),
	
	DADOS_INVALIDOS("Dados inválidos", "/dados-invalidos");
	
	private String title;

	private String uri;

	ProblemType(String title, String path) {
		this.uri = "https://localhost:8080" + path;
		this.title = title;
	}
	
}