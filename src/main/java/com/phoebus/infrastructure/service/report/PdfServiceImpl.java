package com.phoebus.infrastructure.service.report;

import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;

import org.springframework.stereotype.Service;

import com.phoebus.domain.service.ReportsService;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service
public class PdfServiceImpl implements ReportsService {

	@Override
	public byte[] emitirRelatorio(String path, Collection<?> collection) {
		try {
			var inputStream = this.getClass().getResourceAsStream(path);
			
			//Parametro passado porque necessito de conversão de data para o formato br, se não da erro de conversão no pdf
			var parametros = new HashMap<String, Object>();
			parametros.put("REPORT_LOCALE", new Locale("pt", "BR"));
			
			//Dados que vão para o pdf
			var dataSource = new JRBeanCollectionDataSource(collection);
			
			JasperPrint jasperPrint;
			jasperPrint = JasperFillManager.fillReport(inputStream, parametros, dataSource);
			return JasperExportManager.exportReportToPdf(jasperPrint);
		} catch (JRException e) {
			throw new ReportException(e.getMessage());
		}
		
	}

}